package com.patronas.revoluttest

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.patronas.revoluttest.domain.model.Rate
import com.patronas.revoluttest.domain.model.ResponseMapper
import com.patronas.revoluttest.front.view.RatesRecycleAdapter
import com.patronas.revoluttest.front.viewmodel.MainViewModel
import com.patronas.revoluttest.front.viewmodel.RatesRepository
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */


class MainViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()
    private lateinit var viewModel: MainViewModel
    private lateinit var repo: RatesRepository
    private lateinit var responseMapper: ResponseMapper
    private lateinit var context: Context

    
    @Before
    fun before() {
        viewModel = MainViewModel()
        repo = RatesRepository()
        responseMapper = ResponseMapper()
        context = mock(Context::class.java)
    }

    @Test
    fun `verify apiRateLivedata returns the correct api response`() {

        responseMapper.firstRate = "EUR"
        responseMapper.rates = getTestRatesList()

        viewModel.updatedApiRateLiveData.postValue(responseMapper.rates)

        assertEquals(responseMapper.rates, viewModel.updatedApiRateLiveData.value)

    }

    @Test
    fun `verify apiRateLivedata value size will be same as api response`() {

        //make api request
        viewModel.requestDates
        //wait for response
        Thread.sleep(3000)

        val apiResultCount = 33 //server always responds with 33 rates
        val apiRateLiveData = viewModel.updatedApiRateLiveData.value?.size
        println("viewmodel size: $apiRateLiveData")

        assertTrue(apiRateLiveData == apiResultCount)

    }

    @Test
    fun `verify selected value updates on TextWather change`() {
        val newInput = "25"
        viewModel.updateFirstRowInputText(newInput)

        assertEquals(newInput, viewModel.selectedValue)

    }


    @Test
    fun `verify adapter is initialized with the correct items`() {
        assertEquals(getTestListAdapter().rates, getTestRatesList())
        assertTrue(getTestListAdapter().itemCount == getTestRatesList().size)
    }

    @Test
    fun `verify that adapter changed primary row data after update`() {
        val someRateName = "PLN"
        val someRateDesc = "pln"
        val someRateValue = "20"
        val someRateIcon = "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/pln.png"

        getTestListAdapter().updateSelectedRow(someRateName, someRateDesc, someRateValue, someRateIcon)
        assertFalse(someRateName == viewModel.selectedRate)
        assertFalse(someRateDesc == viewModel.selectedDesc)
        assertFalse(someRateValue == viewModel.selectedValue)
        assertFalse(someRateIcon == viewModel.selectedIcon)

    }


    private fun getTestRatesList(): ArrayList<Rate> {
        val rate1 = Rate("name", "desc", "1", "url")
        val rate2 = Rate("name", "desc", "1", "url")
        return arrayListOf(rate1, rate2)
    }

    private fun getTestListAdapter(): RatesRecycleAdapter {
        return RatesRecycleAdapter(
            rates = getTestRatesList(),
            context = context,
            primaryRateName = viewModel.selectedRate,
            primaryRateDesc = viewModel.selectedDesc,
            primaryRateValue = viewModel.selectedValue,
            primaryIcon = viewModel.selectedIcon,
            onClickListener = { _, _ -> }
        )
    }

}
