package com.patronas.revoluttest.data.api

import com.patronas.revoluttest.data.model.RatesApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices{

    @GET("/latest")
    fun getRates(
        @Query("base") currency: String
    ): Call<RatesApiResponse>

}