package com.patronas.revoluttest.data.model

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken



class RatesApiResponse {

    @SerializedName("base")
    val base: String? = null

    @SerializedName("date")
    val date: String? = null

    @SerializedName("rates")
    val rates: JsonObject? = null



    fun getRatesHashMap(): HashMap<String, Double>?{

        return Gson().fromJson<HashMap<String, Double>>(
            rates, object : TypeToken<HashMap<String, Double>>() {

            }.type
        )

    }

}


