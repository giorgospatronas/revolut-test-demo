package com.patronas.revoluttest.domain.model

import com.google.gson.annotations.SerializedName

data class Rate (@SerializedName("currencyName") val rateName: String?,
                 @SerializedName("currencyDesc") val rateDesc: String?,
                 @SerializedName("currencyValue") var rateValue: String?,
                 @SerializedName("currencyIcon")var icon: String?)