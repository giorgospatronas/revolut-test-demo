package com.patronas.revoluttest.domain.mapper

import com.patronas.revoluttest.data.model.RatesApiResponse
import com.patronas.revoluttest.domain.model.Rate
import com.patronas.revoluttest.domain.model.ResponseMapper

class RatesResponseDomainMapper {

    fun apiToDomain(
        apiResponse: RatesApiResponse,
        selectedValue: String,
        selectedIcon: String
    ): ResponseMapper {

        val responseMapper = ResponseMapper()

        responseMapper.firstRate = apiResponse.base


        val calculator = RateHelper(selectedValue)
        responseMapper.rates = calculator.getCalculatedApiAmounts(apiResponse.getRatesHashMap())


        //add selected rate as first item
        val firstRate = responseMapper.firstRate?.let { Rate(it, it, selectedValue, selectedIcon) }
        if(responseMapper.rates != null && firstRate!=null){
            responseMapper.rates!!.add(0, firstRate)
        }

        return responseMapper

    }


}