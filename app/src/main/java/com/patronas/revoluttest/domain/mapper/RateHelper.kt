package com.patronas.revoluttest.domain.mapper

import com.patronas.revoluttest.domain.model.Rate
import java.util.*
import kotlin.collections.ArrayList

class RateHelper(private val selectedValue: String) {

    fun getCalculatedApiAmounts(apiRates: HashMap<String, Double>?): ArrayList<Rate> {

        val rates = ArrayList<Rate>()

        apiRates?.forEach { (key, value) ->

            val multipliedValue = value * selectedValue.toDouble()
            val finalRoundedValue = roundValue(multipliedValue)

            rates.add(Rate(key, key, finalRoundedValue, getRateIconUrl(key)))

        }

        return rates

    }


    private fun roundValue(value: Double): String {
        var amount: String

        try {
            amount = String.format(Locale.ENGLISH, "%.2f", value)
            when (amount) {
                "0.00" -> return "0"
            }
        }catch (e: Exception){
            amount = value.toString()
        }

        return amount
    }

    private fun getRateIconUrl(key: String): String{
        //this url is from a github project with currencies flags
        return "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/" + key.toLowerCase(Locale.ENGLISH) + ".png"
    }

}