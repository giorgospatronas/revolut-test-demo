package com.patronas.revoluttest.front.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.patronas.revoluttest.BuildConfig
import com.patronas.revoluttest.data.api.ApiServices
import com.patronas.revoluttest.data.api.RetrofitRequest
import com.patronas.revoluttest.data.model.RatesApiResponse
import com.patronas.revoluttest.domain.mapper.RatesResponseDomainMapper
import com.patronas.revoluttest.domain.model.Rate
import com.patronas.revoluttest.domain.model.ResponseMapper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RatesRepository {

    val _updatedApiRateLiveData = MutableLiveData<List<Rate>>()
    private val apiServices = RetrofitRequest.getRetrofitInstance().create(ApiServices::class.java)
    var ratesList: ArrayList<Rate> = ArrayList()
    lateinit var responseMapper: ResponseMapper



    fun requestRates(
        selectedRate: String,
        selectedValue: String,
        selectedIcon: String
    ) {
        val call: Call<RatesApiResponse> = apiServices.getRates(selectedRate)
        call.enqueue(object : Callback<RatesApiResponse> {
            override fun onResponse(
                call: Call<RatesApiResponse>,
                response: Response<RatesApiResponse>
            ) {
                val rates = response.body()

                rates?.let {
                     responseMapper =
                        RatesResponseDomainMapper().apiToDomain(rates, selectedValue, selectedIcon)

                    val result = responseMapper.rates
                    _updatedApiRateLiveData.postValue(result)
                    ratesList = result!!//update list to notifyadapter
                }

            }

            override fun onFailure(call: Call<RatesApiResponse>, t: Throwable) {
                if (BuildConfig.DEBUG) {
                    Log.i("response", "error")
                }
            }
        })
    }



}

