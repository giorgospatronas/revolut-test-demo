package com.patronas.revoluttest.front.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.patronas.revoluttest.domain.model.Rate
import java.util.*
import kotlin.math.roundToInt


class MainViewModel : ViewModel() {

    var selectedRate: String = "EUR"
    var selectedDesc: String = "eur"
    var selectedValue: String = "1"
    var selectedIcon: String = "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/eur.png"
    val REQUEST_INTERVAL: Long = 1000


    private val ratesRepo: RatesRepository =
        RatesRepository()

    val updatedApiRateLiveData: MutableLiveData<List<Rate>>
        get() = ratesRepo._updatedApiRateLiveData

    val requestDates
        get() = ratesRepo.requestRates(selectedRate, selectedValue, selectedIcon)

    val ratesList
        get() = ratesRepo.ratesList


    fun moveItemToTop(fromPos: Int, toPos: Int) {
        Collections.swap(ratesList, fromPos, toPos)
    }

    fun updateFirstRowInputText(s: CharSequence?) {

        s?.let {
            val amount: String = when {
                s.isEmpty() -> (0.0).roundToInt().toString()
                else -> s.toString()
            }

            selectedValue = amount
        }

    }

}