package com.patronas.revoluttest.front.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.patronas.revoluttest.R
import com.patronas.revoluttest.domain.model.Rate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycle_list_item_rate.view.*


open class RatesRecycleAdapter(
    var rates: ArrayList<Rate>,
    private val context: Context,
    private var primaryRateName: String,
    private var primaryRateDesc: String,
    private var primaryRateValue: String,
    private var primaryIcon: String,
    private val onClickListener: (View, Int) -> Unit
) :
    RecyclerView.Adapter<RatesRecycleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.recycle_list_item_rate,
                parent,
                false
            )
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txtCurrencyLabel.text = rates[position].rateName

        holder.itemView.setOnClickListener { view ->
            onClickListener(view, holder.adapterPosition)
        }

        holder.currencyDesc.text = rates[position].rateDesc

        Picasso.get().load(rates[position].icon).into( holder.currencyIcon)


        when {
            holder.adapterPosition == 0 -> {
                holder.txtCurrencyLabel.text = primaryRateName//first row will not be updated, so when it is recycled it should containt user's last input
                holder.currencyDesc.text = primaryRateDesc
                holder.currencyValueInput.setText(primaryRateValue)
                Picasso.get().load(primaryIcon).into( holder.currencyIcon)
            }
            else -> holder.currencyValueInput.setText(rates[position].rateValue)
        }


    }


    override fun getItemCount(): Int {
        return rates.size
    }


    fun updateData(newRates: ArrayList<Rate>) {
        rates = newRates
        notifyDataSetChanged()
    }

    fun updateItemsRange(from: Int, range: Int, newRates: ArrayList<Rate>) {
        for (i in 1 until newRates.size) {
            rates[i] = newRates[i]
        }
        notifyItemRangeChanged(from, range)
    }

    fun updateSelectedRow(newRateName: String, newRateDesc: String, newAmount: String, newIcon: String) {
        primaryRateName = newRateName
        primaryRateDesc = newRateDesc
        primaryRateValue = newAmount
        primaryIcon = newIcon
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val txtCurrencyLabel: TextView = view.txt_currency_label
        val currencyValueInput: EditText = view.edittext_currency_amount
        val currencyIcon: ImageView = view.img_flag
        val currencyDesc: TextView = view.txt_currency_desc

    }

}


