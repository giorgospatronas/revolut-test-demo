package com.patronas.revoluttest.front.view

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.patronas.revoluttest.R
import com.patronas.revoluttest.domain.model.Rate
import com.patronas.revoluttest.front.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.recycle_list_item_rate.view.*


class MainActivity : AppCompatActivity() {

    private lateinit var recycleView: RecyclerView
    private lateinit var adapter: RatesRecycleAdapter
    private var mainHandler: Handler? = null
    private lateinit var viewModel: MainViewModel

    private val updatedApiRateObserver =  androidx.lifecycle.Observer<List<Rate>> {
        //on 1st date fetch init adapter
        if (adapter.itemCount == 0) {
            adapter.updateData(it as ArrayList<Rate>)

        } else {//on every data update notify all items except first
            adapter.updateItemsRange(1, it.size, it as ArrayList<Rate>)
        }

        //register listener to 1st row
        registerFirstInputListener()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        initViews()

    }

    override fun onResume() {
        super.onResume()
        startObservingApi()
    }

    override fun onPause() {
        super.onPause()
        stopObservingApi()
    }


    private fun initViews() {
        recycleView = findViewById(R.id.recycle_rates_list)
        setUpRecycleAdapter(viewModel.ratesList)
    }

    private fun setUpRecycleAdapter(rates: ArrayList<Rate>) {

        val llm = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        recycleView.layoutManager = llm

        adapter = RatesRecycleAdapter(
            rates = rates,
            context = this,
            primaryRateName = viewModel.selectedRate,
            primaryRateDesc = viewModel.selectedDesc,
            primaryRateValue = viewModel.selectedValue,
            primaryIcon = viewModel.selectedIcon,
            onClickListener = { view, position -> viewClickListener(view, position) }
        )

        recycleView.adapter = adapter


    }


    private fun startMonitoringRates() {

        mainHandler = Handler(Looper.getMainLooper())

        mainHandler?.post(object : Runnable {
            override fun run() {
                viewModel.requestDates
                mainHandler?.postDelayed(this, viewModel.REQUEST_INTERVAL)
            }
        })

        //observer for API changed data
        viewModel.updatedApiRateLiveData.observe(this, updatedApiRateObserver)
    }


    private fun setFocus(input: EditText?) {
        input?.let {
            input.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(input, SHOW_IMPLICIT)
        }
    }


    /**
     * UI, adapter listeners
     */


    private fun registerFirstInputListener() {

        adapter.updateSelectedRow(viewModel.selectedRate, viewModel.selectedDesc, viewModel.selectedValue, viewModel.selectedIcon)//keep adapter viewHolder updated for the 1st row
        val firstHolder = recycleView.findViewHolderForAdapterPosition(0)
        val input = firstHolder?.itemView?.edittext_currency_amount

        input?.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(10))

        input?.removeTextChangedListener(textWatcher)
        input?.addTextChangedListener(textWatcher)

    }

    private val textWatcher = (object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(
            s: CharSequence?,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (!recycleView.isComputingLayout) {
                viewModel.updateFirstRowInputText(s)
            }
        }

    })

    private fun viewClickListener(view: View?, position: Int) {//change row order on user click

        //update requesτ parameter
        viewModel.selectedRate = viewModel.ratesList[position].rateName.let{viewModel.ratesList[position].rateName!!}
        viewModel.selectedDesc = viewModel.ratesList[position].rateDesc.let{viewModel.ratesList[position].rateDesc!!}
        viewModel.selectedValue = viewModel.ratesList[position].rateValue.let{viewModel.ratesList[position].rateValue!!}
        viewModel.selectedIcon = viewModel.ratesList[position].icon.let{viewModel.ratesList[position].icon!!}

        //move item to top
        viewModel.moveItemToTop(position, 0)

        adapter.notifyItemMoved(position, 0)
        recycleView.scrollToPosition(0)

        val input: EditText? = view?.findViewById(R.id.edittext_currency_amount)

        if (position != 0) {
            setFocus(input)
        }

        registerFirstInputListener()
    }

    private fun stopObservingApi(){
        mainHandler?.removeCallbacksAndMessages(null)
        mainHandler = null
    }

    private fun startObservingApi(){
        startMonitoringRates()
    }

}
